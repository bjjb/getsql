# getsql

> /ɡˈɛt/ /skwˈiːl/

A little Crystal library you can use to reason about databases, particularly
which indices they have, and to make `SELECT` queries which should use those
indices. Its intended use is for quick, throw-away REST APIs when a database
already exists.

There's also the `getsql` command-line interface, with sub-commands to query a
database's availability, tables and indices using its URI, as well as to start
an API server which connects to and provides endpoints for as many DBs as you
like.

> 🚧 This library is in an early stage of development, and shouldn't be relied
> on for production workloads. In fact, production workloads aren't really its
> purpose. If 💩 blows up, that's on you. 🤷

## Installation

Grab a [release][] for your architecture and put it in your `$PATH`. Or
build it (using [crystal][]).

### From source

You'll need [crystal][]. You'll also probably need libraries for SQlite3 and
PostgreSQL (most package manager 
You can clone [the repo](https://gitlab.com/bjjb/getsql) and run

    shards build

The executable will appear in `bin/getsql`. To replicate a release, use

    shards build --production --static

on a specific tag.

## Usage

    getsql --help

Or you can start the service (with `getsql start`) and use a HTTP client to
interact with it. See the [API docs][api] for details.

### In your projects

Add the following to your shards.yml:

```yaml
dependencies:
  getsql:
    gitlab: bjjb/gitsql
```

and run `shards install`.

## Development

    crystal tool format
    crystal spec

This library works with databases, so there are groups of specs tagged with
`sqlite3`, `postgres`, etc. You can skip these (or focus on them), if you want
to, with tags - see `crystal spec --help` for how. By default, it'll expect to
connect to `postgres:postgres:postgres//localhost:5432/postgres`.  You can use
a different Postgres, if you need to, with the `PG...` environment variables,
like psql(1) does - the specs will check these. (The spec-suite then creates a
_new_ role and schema for each test, uses them to test behaviour, and drops
them afterwards.  If a spec crashes, you might need to clean up manually.) No
special behaviour is needed for `sqlite3` specs - they just use a temporary
file.

## Contributing

1. [Fork it][fork]
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [bjjb](https://gitlab.com/@bjjb) - creator and maintainer

[crystal]: https://crystal-lang.org
[releases]: https://gitlab.com/bjjb/getsql/-/releases
[fork]: https://gitlab.com/bjjb/getsql/-/forks/new
