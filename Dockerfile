FROM crystallang/crystal:1.11.2-alpine AS builder
RUN apk add --no-cache git ca-certificates sqlite-dev sqlite-static libpq-dev
RUN adduser -S getsql
ADD . .
RUN shards build --production --static

FROM scratch
COPY --from=builder /bin/getsql /bin/getsql
COPY --from=builder /etc/passwd /etc/
COPY --from=builder /etc/group /etc/
EXPOSE 8080
USER getsql:nogroup
CMD ["/bin/getsql", "-d"]
