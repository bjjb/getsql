require "spec"
require "../src/getsql"

describe GetSQL do
  name = "getsql_spec_#{Time.utc.to_s("%s")}"

  describe "postgres", tags: "postgres" do
    urlfmt = "postgres://%{user}:%{pass}@%{host}:%{port}/%{db}"
    pending unless admin = URI.parse(urlfmt % {
                     user: ENV.fetch("PGUSER", "postgres"),
                     pass: ENV.fetch("PGPASSWORD", "postgres"),
                     host: ENV.fetch("PGHOST", "localhost"),
                     port: ENV.fetch("PGPORT", "5432"),
                     db:   ENV.fetch("PGDATABASE", "postgres"),
                   })
    test = URI.parse(urlfmt % {
      user: name,
      pass: name,
      host: ENV.fetch("PGHOST", "localhost"),
      port: ENV.fetch("PGPORT", "5432"),
      db:   name,
    })

    before_all do
      DB.open(admin) do |db|
        db.exec "CREATE ROLE #{name} LOGIN PASSWORD '#{name}'"
        db.exec "CREATE DATABASE #{name} OWNER #{name}"
      end

      DB.open(test) do |db|
        db.exec <<-SQL
          CREATE TABLE IF NOT EXISTS foo (
            id INTEGER PRIMARY KEY,
            owner TEXT NOT NULL,
            data JSONB NOT NULL DEFAULT '{}'::jsonb,
            created_at TIMESTAMP NOT NULL DEFAULT now()
          )
        SQL
        db.exec <<-SQL
          CREATE TABLE IF NOT EXISTS bar (
            id INTEGER PRIMARY KEY,
            foo INTEGER NOT NULL,
            data JSONB NOT NULL DEFAULT '{}'::jsonb,
            created_at TIMESTAMP NOT NULL DEFAULT now()
          )
        SQL
        db.exec "CREATE INDEX foo_id_owner ON foo (id, owner)"
        db.exec "CREATE INDEX foo_owner_created_at ON foo (owner, created_at)"
        db.exec "CREATE INDEX bar_id_foo ON bar (id, foo)"
        db.exec "CREATE INDEX bar_foo_created_at ON bar (foo, created_at)"
        db.exec %(INSERT INTO foo (id, owner, data) VALUES (1, 'alice', '{"a":1}'::jsonb))
        db.exec %(INSERT INTO foo (id, owner, data) VALUES (2, 'bob', '{"b":2}'::jsonb))
        db.exec %(INSERT INTO bar (id, foo, data) VALUES (1, 1, '{"x":1}'::jsonb))
        db.exec %(INSERT INTO bar (id, foo, data) VALUES (2, 2, '{"x":2}'::jsonb))
      end
    end

    after_all do
      DB.open(admin) do |db|
        db.exec("DROP DATABASE #{name}")
        db.exec("DROP ROLE #{name}")
      end
    end

    it "can list all tables in a database" do
      GetSQL.tables(test).should eq %w(foo bar)
    end

    it "can list all columns in a table" do
      GetSQL.columns(test, "foo").sort.should eq %w(created_at data id owner)
      GetSQL.columns(test, "bar").sort.should eq %w(created_at data foo id)
    end

    it "can list all indices associated with a table" do
      GetSQL.indices(test, "foo").should eq [%w(id), %w(id owner), %w(owner created_at)]
      GetSQL.indices(test, "bar").should eq [%w(id), %w(id foo), %w(foo created_at)]
    end
  end

  describe "sqlite3", tags: "sqlite3" do
    file = File.join(Dir.tempdir, "#{name}.sqlite3")
    test = URI.parse("sqlite3://#{file}")

    before_all do
      DB.open(test) do |db|
        db.exec <<-SQL
          CREATE TABLE IF NOT EXISTS foo (
            id INTEGER PRIMARY KEY,
            owner TEXT NOT NULL,
            data TEXT NOT NULL DEFAULT '{}',
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
          )
        SQL
        db.exec <<-SQL
          CREATE TABLE IF NOT EXISTS bar (
            id INTEGER PRIMARY KEY,
            foo INTEGER NOT NULL,
            data TEXT NOT NULL DEFAULT '{}',
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
          )
        SQL
        db.exec "CREATE INDEX foo_id_owner ON foo (id, owner)"
        db.exec "CREATE INDEX foo_owner_created_at ON foo (owner, created_at)"
        db.exec "CREATE INDEX bar_id_foo ON bar (id, foo)"
        db.exec "CREATE INDEX bar_foo_created_at ON bar (foo, created_at)"
        db.exec %(INSERT INTO foo (id, owner, data) VALUES (1, 'alice', '{"a":1}'))
        db.exec %(INSERT INTO foo (id, owner, data) VALUES (2, 'bob', '{"b":2}'))
        db.exec %(INSERT INTO bar (id, foo, data) VALUES (1, 1, '{"x":1}'))
        db.exec %(INSERT INTO bar (id, foo, data) VALUES (2, 2, '{"x":2}'))
      end
    end

    after_all do
      File.delete(file)
    end

    it "can list all tables in a database" do
      GetSQL.tables(test).should eq %w(foo bar)
    end

    it "can list all columns in a table" do
      GetSQL.columns(test, "foo").should eq %w(id owner data created_at)
      GetSQL.columns(test, "bar").should eq %w(id foo data created_at)
    end

    it "can list all indices associated with a table" do
      GetSQL.indices(test, "foo").should eq [%w(id owner), %w(owner created_at)]
      GetSQL.indices(test, "bar").should eq [%w(id foo), %w(foo created_at)]
    end

    it "can get records from a database using an index" do
      results = GetSQL.query(test, "foo", limit: 2)
      results.size.should eq 2
      results[0].keys.sort.should eq %w(created_at data id owner)
      results[0]["id"].should eq 1
      results[0]["data"].should eq %({"a":1})
      JSON.parse(results[0]["data"].to_s).should eq({"a" => 1})
    end
  end
end
