require "option_parser"
require "yaml"
require "./getsql"

OptionParser.parse do |p|
  app = GetSQL.new

  p.banner = "Usage: $PROGRAM_NAME [options]"

  p.on "-h", "--help", "print this" do
    p.stop
    p.unknown_args { puts p }
  end

  p.on "-V", "--version", "print version info" do
    p.stop
    p.unknown_args { puts "getsql v#{app.version}" }
  end

  p.on "-c", "--connection URI", "add a connection" do |uri|
    app.add(uri)
  end

  p.on "-f", "--connections FILE", "add connections from a file" do |file|
    File.open(File.expand_path(file, home: true)) do |f|
      Hash(String, String).from_yaml(f).each do |name, uri|
        app.add(name, URI.parse(uri))
      end
    end
  end

  p.on "-d", "--daemon", "start a server" do
    p.unknown_args do
      puts "Listening on :8080..."
      HTTP::Server.new(app).tap do |server|
        Signal::INT.trap { server.close unless server.closed? }
      end.listen(ENV.fetch("PORT", "8080").to_i(10))
      puts "Goodbye!"
    end
  end

  p.on "tables", "lists the tables in databases" do
    p.unknown_args do |args, cmd|
      app.databases.each do |db|
        app.tables(db).map { |t| puts("#{db} #{t}") }
      end
    end
  end

  p.unknown_args do |args, cmd|
    app.databases.each do |name|
      puts "%s %s" % [name, app.ok?(name)]
    end
  end
end
