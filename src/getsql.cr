require "db"
require "pg"
require "sqlite3"
require "semantic_version"

# GetSQL provides functions for extracting meta-information from a databases
# in a generic fashion, and for making requests.
class GetSQL
  include HTTP::Handler

  # An error specific raised by this library
  class Error < Exception
  end

  # Raised when a database or table cannot be found
  class NotFound < Error
  end

  # Each entry is a query to return single column rows with the names of all
  # tables in a database.
  TABLES = {
    "sqlite3"  => "SELECT tbl_name FROM sqlite_schema WHERE type = 'table'",
    "postgres" => "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = 'public'",
  }

  # Each entry is a query to return 2-column rows, with the name of a table and
  # the definition of an index.
  INDICES = {
    "sqlite3"  => "SELECT sql FROM sqlite_schema WHERE type = 'index' AND tbl_name = '%s'",
    "postgres" => "SELECT indexdef FROM pg_indexes WHERE schemaname = 'public' AND tablename = '%s'",
  }

  COLUMNS = {
    "sqlite3"  => "SELECT name FROM pragma_table_info('%s')",
    "postgres" => "SELECT column_name FROM information_schema.columns WHERE table_name = '%s'",
  }

  # The default content type
  JSON = MIME.from_extension(".json")

  def initialize(connections = {} of String => String)
    @connections = connections.reduce({} of String => URI) do |acc, (k, v)|
      acc.tap { |acc| acc[k] = URI.parse(v.to_s) }
    end
  end

  @connections : Hash(String, URI)

  # True if and only if the database at the URI corresponding to the given name
  # can be reached.
  def ok?(name : String)
    self.class.ok?(@connections[name])
  end

  # Adds a new connection with the given name.
  def add(name : String, uri : URI)
    DB.open(uri) { @connections[name] = uri }
  end

  # Adds a new connection with the given name.
  def add(name : String, uri : String)
    add(name, URI.parse(uri))
  end

  # Adds a new connection named by the first element in the URI's path
  # (excluding any file extension)
  def add(uri : URI)
    uri.path.strip('/').split('/').first.tap do |name|
      add(File.basename(name, File.extname(name)), uri)
    end
  end

  # Adds a new connection named by the first element in the URI's path
  # (excluding any file extension)
  def add(uri : String)
    add(URI.parse(uri))
  end

  # Removes the named connection, if it exists.
  def remove(name : String)
    @connections.delete(name)
  end

  def databases
    @connections.keys.sort
  end

  def database(name)
    @connections[name]
  end

  def version
    SemanticVersion.new(0, 0, 0)
  end

  def call(context)
    case context.request.method.upcase
    when "GET"  then get(context)
    when "HEAD" then head(context)
    else             method_not_allowed(context)
    end
  end

  def respond(context)
    context.response.content_type = MIME.from_extension(".json")
    yield.to_json(context.response)
  end

  # Gets the tables in the database specified by `uri`.
  def tables(db : String)
    self.class.tables(database(db))
  end

  # Gets the indices in the database, as a map of index definitions per table.
  def indices(db : String, table : String)
    self.class.indices(database(db), table)
  end

  def query(db : String, table : String, params : Enumerable({String, String})? = nil, limit : Int = 1, offset : Int = 0)
    self.class.query(database(db), table, params, limit, offset)
  end

  private def get(context)
    path = context.request.path.strip('/')
    respond(context) do
      next databases if path == ""
      next tables(path) if path =~ %r<\A[^/]+\z>
      if path =~ %r<\A[^/]+/[^/]+\z>
        db, table = path.split('/')
        if context.request.query_params.empty?
          next indices(db, table)
        end
      end
      raise NotFound.new(path)
    end
  rescue e
    context.response.respond_with_status(HTTP::Status::BAD_REQUEST)
  end

  private def head(context)
    response = context.response
    response.respond_with_status(HTTP::Status::NOT_FOUND)
  end

  private def method_not_allowed(context)
    context.response.respond_with_status(HTTP::Status::METHOD_NOT_ALLOWED)
  end

  # Gets a redacted version of the URI (with the password obfuscated).
  def self.redacted(uri : URI | String) : URI
    URI.parse(uri.to_s).tap(&.password=(nil))
  end

  # True if and only if the database at the URI can be reached.
  def self.ok?(uri : URI)
    DB.open(uri) { true } rescue false
  end

  def self.tables(uri : URI)
    DB.open(uri, &.query_all(TABLES[uri.scheme], as: String))
  end

  def self.indices(uri : URI, table : String)
    parse = ->(s : String) { s.match(/\(([^)]+)\)/).try(&.[1].split(/\s*,\s*/)) }
    DB.open(uri, &.query_all(INDICES[uri.scheme] % table, as: String)).map(&parse)
  end

  def self.columns(uri : URI, table : String)
    DB.open(uri, &.query_all(COLUMNS[uri.scheme] % table, as: String))
  end

  def self.column_type(uri : URI, table : String, column : String)
    # TODO find a sensible type for the column
    DB::Any
  end

  def self.query(uri : URI, table : String, params : Enumerable({String, String})? = nil, limit : Int = 1, offset : Int = 0)
    DB.open(uri) do |db|
      Array(Hash(String, DB::Any)).new.tap do |results|
        db.query_each("SELECT * FROM #{table} #{where(params)} LIMIT ? OFFSET ?", limit, offset) do |rs|
          Hash(String, DB::Any).new.tap do |row|
            rs.each_column do |c|
              row[c] = rs.read(column_type(uri, table, c))
            end
            results << row
          end
        end
      end
    end
  end

  def self.where(params : Enumerable({String, String})?)
    return "" if params.nil?
    "WHERE " + params.map { |k, v| [k, "'#{v}'"].join('=') }.join(" AND ")
  end
end
