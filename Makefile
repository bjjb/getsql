.PHONY: spec format check

spec:
	@crystal spec

format:
	@crystal tool format

check:
	@crystal tool format --check
	@crystal spec --fail-fast
	@shards build -q
	@npx @redocly/cli lint openapi.yaml
	@glab ci lint
